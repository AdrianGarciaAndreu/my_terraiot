
//////////////////////
///// Variables //////
//////////////////////


//WiFi API URLs
String N_GET = "http://us-central1-es-myterra.cloudfunctions.net/app/api/read";
String N_PUT = "http://us-central1-es-myterra.cloudfunctions.net/app/api/update";
String N_POST = "http://us-central1-es-myterra.cloudfunctions.net/app/api/create";
String N_DELETE = "http://us-central1-es-myterra.cloudfunctions.net/app/api/delete";

StaticJsonDocument<2048> jsonR; //JSON recieved
StaticJsonDocument<2048> jsonS; //JSON to send



float temp = 0;
float HR = 0;
int moist = 0;
int lightInt = 0;
bool ledState = false; 
byte ledColors[3] = {100, 100, 100};
int wInt = 0;
bool UVState = false;
String _name = "Node name";







////////////////////
//// Sensors ///////
////////////////////

//SENSOR Libraries
#include <DHT.h>

//SENSOR PIN
#define DHTPIN 27  // Digital pin connected to the DHT sensor
#define MOISTPIN 34 //Pin analógico para el sensor de humedad (resistivo)
#define LINTENSITYPIN 35 //Pin analógico para el sensor de intesidad lumínica
#define UVCONTROL 32 // Pin para control de UV (Digital también puede servir)




//SENSOR Calibrations and others
#define DHTTYPE DHT11

//Moist Analog
#define moist_wet 1650
#define moist_dry 3510

//Light Analog
#define dark_value 100
#define clear_value 1500 //2100



DHT tempSensor(DHTPIN, DHTTYPE);



void initializeSensors(){
  tempSensor.begin();
  
  strip.Begin(); delay(500);
  strip.ClearTo(_black);
  strip.Show();

}



/*
 * Writes son text via BT
 */
void WriteBT(const unsigned char msg[], int msgSize){

 const unsigned char * const p = msg;

  SerialBT.write(p, msgSize);

  delay(500);
}



void retrieveTemp(){
  
  float t = tempSensor.readTemperature();
  delay(1000);
  float h = tempSensor.readHumidity();
  
  //Serial.print("Temperature = ");
  //Serial.println(t);

  if(t==NULL || isnan(t)){t=0;}
  if(h==NULL || isnan(h)){h=0;}
  
  temp = t;
  HR = h;
  
}


void readMoist(int pin_num, int _min, int _max){
  int val = analogRead(pin_num); //connect sensor to Analog 0
  val = map(val, moist_dry, moist_wet, 0, 100);
  if(val<_min){val = 0;}
  else if(val > _max){ val = 100;}

  moist = val;
}


void processLights(){

  RgbwColor _color(ledColors[0], ledColors[1], ledColors[2], wInt);
  for (int i = 0; i < LEDNUM; i++) {
    strip.SetPixelColor(i, _color);
  }
   
   strip.Show();
  
}


void clearLights(){

  for (int i = 0; i < LEDNUM; i++) {
    //strip.SetPixelColor(i, 15, 1, 1, 0);
    strip.SetPixelColor(i, _black);

  }
  
}



void readLux(){
  int light = analogRead(LINTENSITYPIN);
  //Serial.print("LIGHT ");Serial.println(light);
  int l_map = map(light, dark_value, clear_value, 0, 100);
  if(l_map<0){l_map = 0;}
  else if(l_map > 100){ l_map = 100;}
  //Serial.print("LIGHT ");Serial.println(l_map);
  lightInt = l_map;

}


void switchOnUV(){
    //Serial.println("Enabling UV");
    digitalWrite(UVCONTROL,HIGH);
}

void switchOffUV(){
    //Serial.println("Disabling UV");
    digitalWrite(UVCONTROL,LOW);
}


void gatherNetStatus(){

    String ack = "[ACK_STATUS]";
    ack += "[WIFI_CONFIGURED]="+isWifiSetUp;
    ack +=isWifiSetUp;
    ack += "[SSID]=";
    ack += ssid;
    ack += "[PASS]=";
    ack += String(ssid);
    ack += "[WIFI_CONNECTED]=";
    ack +=connectedWifi;
    ack += "[END]";
     
}


void gatherData(){
  retrieveTemp();
  readMoist(MOISTPIN, 0, 100);
  readLux();
}



//////////////////////////////
///// Connections ////////////
//// Data processing /////////
//////////////////////////////




//Cloud data writting (updates the current status)
void writeCloudStatus(){
    
    //Populate JSON Document
    jsonS.clear();
    StaticJsonDocument<2048> jsonItemData;
        
    unsigned long _seconds = getMoment(ntpServerName);
    
    jsonItemData["momment"] = _seconds;
    jsonItemData["hwMomment"] = _seconds;    
    jsonItemData["temp"] = temp;
    jsonItemData["hr"] = HR;
    jsonItemData["lightInt"] = lightInt;
    jsonItemData["moist"] = moist;


   
    jsonS["item"] = jsonItemData;
    
    String jsonString;
    serializeJson(jsonS, jsonString); //parse from retrieved String to JSON object
    
    
    String n_put = N_PUT + "/"+WiFi.macAddress();

    #ifdef DEBUG_REQUESTS
      Serial.println("Data to PUT:");
      Serial.println(jsonString);
    #endif
    httpPUTRequest(&n_put[0], jsonString);

}








//Cloud data reading (first to check the previous status (LEDs))
void readCloudStatus(){

  String mac_addr = WiFi.macAddress();
  String readings;

  String n_get = N_GET + "/"+mac_addr;
  readings = httpGETRequest(&n_get[0]);
  //Serial.println(readings + " Legnth: "+readings.length());

  if(readings.length()<1 || readings.equals("Not exists")){
   
    
    //No exist
    //Populate JSON Document
    jsonS.clear();
    StaticJsonDocument<2048> jsonItemData;

    _name = _name + " "+mac_addr;
    unsigned long _seconds = getMoment(ntpServerName);
        
    jsonItemData["momment"] = _seconds;
    jsonItemData["hwMomment"] = _seconds;    

    
    jsonItemData["name"] = _name;
    jsonItemData["id"] = mac_addr;
    jsonItemData["temp"] = temp;
    jsonItemData["hr"] = HR;
    jsonItemData["moist"] = moist;
    jsonItemData["lightInt"] = lightInt;
    jsonItemData["ledState"] = ledState;
    JsonArray jcolors = jsonItemData.createNestedArray("ledColor");
      jcolors.add(ledColors[0]);  //Red
      jcolors.add(ledColors[1]);  //Green
      jcolors.add(ledColors[2]);  //Blue
    jsonItemData["uvState"] = ledState;
    jsonItemData["guid"] = guid.c_str();
    jsonItemData["wInt"] = wInt;
    
    jsonS["id"] = mac_addr.c_str();
    jsonS["item"] = jsonItemData;
    
    String jsonString;
    serializeJson(jsonS, jsonString); //parse from retrieved String to JSON object
    
    httpPOSTRequest(&N_POST[0], jsonString);

    Serial.println("Creating node");
    Serial.println(jsonString);
    Serial.println("");
    
  }else{
    
    DeserializationError error = deserializeJson(jsonR, readings); //parse from retrieved String to JSON object
    if (error) {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.c_str());
      return;
    }

    Serial.println("Reading node...");
    
    Serial.print("Data: "); Serial.println(readings);
    Serial.println("");
    
    //ProcessData //Lights and others
    JsonArray rcolors = jsonR["ledColor"];
    ledColors[0] = rcolors[0];
    ledColors[1] = rcolors[1];
    ledColors[2] = rcolors[2];

    wInt = jsonR["wInt"]; //intensity for W in GRBW WS2812b LEDs IC
    

    if(boolean(jsonR["ledState"])){
     processLights(); 
    } else{

      Serial.println("CLEARING...");
      Serial.println("");
      
      strip.ClearTo(_black);
      strip.Show();
      //clearLights();
    }
    
    if(boolean(jsonR["uvState"])){
      switchOnUV();
    } else{
      switchOffUV();
      }


    if(boolean(jsonR["statusRequest"])){
      gatherNetStatus();
    }

    //Sensor readings //Update sensors
    gatherData();


    //UpdateNode
    writeCloudStatus();
  }
 
  
  
}







///////////////// Bluetooth




void BLEParser(String _data){


  //Network statement
  if(_data.indexOf("[NET]")>-1){
    int _st, _sp;
    
    _st = (_data.indexOf("[SSID]")) + (strlen("[SSID]")); _sp = (_data.indexOf("[PASS]"));
    int _sz = (_sp-_st);
    String _ssid = "";
    for(int i=_st; i<_sp; i++){
      _ssid += _data[i];
    }
    

    //password
    _st = (_data.indexOf("[PASS]")) + (strlen("[PASS]")); _sp = (_data.indexOf("[GUID]"));
    _sz = (_sp-_st);
    String _pass = "";
    
    for(int i=_st; i<_sp; i++){
      _pass += _data[i];
    }

    //gF34xpdqZLVUxwCUPtXht0d57R52

    //GUID
    _st = (_data.indexOf("[GUID]")) + (strlen("[GUID]")); _sp = (_data.indexOf("[END]"));
    _sz = (_sp-_st);
    String _guid = "";
    
    for(int i=_st; i<_sp; i++){
      _guid += _data[i];
    }

    guid = _guid; //Guardo la GUID

    char m_ssid[_ssid.length() + 1]; 
    char m_pass[_pass.length() + 1];
    
    strcpy(m_ssid, _ssid.c_str());
    strcpy(m_pass, _pass.c_str());


    ssid = &m_ssid[0];
    password = &m_pass[0];

    String ack = "[ACK_OK]";
    
    WriteBT((const unsigned char *)ack.c_str(), ack.length());
    
    attemptToConnect();
    
    
  } //END OF NET REQUEST
  
  if(_data.indexOf("[STATUS]")>-1){ //STATUS REQUEST
    String ack = "[ACK_STATUS]";
    ack += "[WIFI_CONFIGURED]="+isWifiSetUp;
    ack +=isWifiSetUp;
    ack += "[SSID]=";
    ack += ssid;
    ack += "[PASS]=";
    ack += String(ssid);
    ack += "[WIFI_CONNECTED]=";
    ack +=connectedWifi;
    ack += "[END]";
   
    WriteBT((const unsigned char *)ack.c_str(), ack.length());


  }

  
}


//Escuchador de datos BLE, en caso de detectar red WiFi, ignora este escuchador el programa
void ListenBLE(){

    if (SerialBT.available()) {
      #ifdef DEBUG
        Serial.println("Data detected! ");
        Serial.print(SerialBT.available());
        Serial.println(" bytes");
      #endif
      
      char buff[2048];
      char c_char; int in_index = 0;
      
      while(SerialBT.available()>0){
          c_char = char(SerialBT.read());
          buff[in_index] = c_char;
          
          in_index++;
          
          delay(20);
      }

     
      //Serial.print("Size: "); Serial.print(in_index,DEC); Serial.println();
      
      char defBuff[in_index]; //Size of all available characters iterated and readed
      for(int i=0;i<=in_index;i++){
        defBuff[i] = buff[i];
      }
      
      String _data(defBuff);
      #ifdef DEBUG_BLE
        Serial.println(); Serial.println(_data);
      #endif
      BLEParser(_data);
      
      
    }
    
}
