

//#define DEBUG
//#define DEBUG_WIFI
//#define DEBUG_REQUESTS
#define DEBUG_BLE

#include <EEPROM.h>
#define EEPROM_SIZE 128 // define the number of bytes you want to access


// Conectividad
#include "BluetoothSerial.h"
#include "WiFi.h"
#include <HTTPClient.h>

#include <WiFiUdp.h>
#include <TimeLib.h>
#include <Timezone.h>

#include <ArduinoJson.h>
//#include <FastLED.h>
//#include <Adafruit_NeoPixel.h>
#include <NeoPixelBus.h>
RgbwColor black(0);

#define LED 2 //Embebbed led
#define RESET_BTN 13


#define LEDPIN 26  // Digital pin connected to the DHT sensor
#define LEDNUM 2


NeoPixelBus<NeoGrbwFeature, Neo800KbpsMethod> strip(LEDNUM, LEDPIN);
RgbwColor _black(0,0,0,0);


#include "Time.h"
#include "Connectivity.h"
#include "DataProcessing.h"


///////// INTERRUPTS ////////////////////

volatile bool rst_state = false;
void deviceReset(){
  rst_state = true;
  
}




////// POLLING //////////////////////// 

void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);
  delay(100);
  Serial.println("Starting...");

  SerialBT.begin("MyTerra Node"); //Bluetooth device name


  pinSetUp();

  attachInterrupt(digitalPinToInterrupt(RESET_BTN),deviceReset,RISING);

  EEPROM.begin(EEPROM_SIZE);
  //EEPROMClear(); //Clear the EEPROM


  initializeSensors();

  getWiFiCredentials();



}

void loop() {

    if (rst_state) {
      rst_state = false;
      Serial.println("Reset device");
        EEPROMClear(); //Clear the EEPROM
        delay(1000);
        ESP.restart();
    }

     
  if(!isWifiSetUp){
    Serial.println("Wifi Not set up");
    ledBlink(); ListenBLE(); //Listen BLE
  }
  else if(!connectedWifi){ //attempt to reconnect in case of failure
   ListenBLE(); //Listen BLE 
   Serial.println("Attempt to connect"); 
    if(connectAttempts<=0){
      ESP.restart();
    }
    connectWithSavedCredentials();
    connectAttempts--;
  }
  else if(connectedWifi){ //credentials readed and wifi connected
    //Serial.println("Conectado al WiFi");
    if( WiFi.status() != WL_CONNECTION_LOST &&
        WiFi.status() != WL_DISCONNECTED && 
        WiFi.status() != WL_CONNECT_FAILED){
      Serial.println("Measuring");
      readCloudStatus(); 
    } else{
      connectedWifi = false; //WIFI LOST
    }
    
    delay(15000);
    //delay(1500);
  }

  delay(10000);
  //Serial.println("Doing nothing");
}




void pinSetUp(){
    pinMode(LED,OUTPUT);
    pinMode(UVCONTROL, OUTPUT);
}





//Resets the EEPROM values
void EEPROMClear(){

  Serial.println("Clearing the EEPROM");

  for (int i = 0; i < EEPROM_SIZE; i++) {
    EEPROM.write(i, 0);
  }

  EEPROM.commit();
  
}
