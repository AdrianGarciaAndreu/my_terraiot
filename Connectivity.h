

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif



BluetoothSerial SerialBT; //Bluetooth object
HTTPClient http; //http object



//WiFi credential
char* ssid = "";
char* password = "";
String guid = "";

bool isWifiSetUp = false;
bool credentialsObtained = false;
bool connectedWifi = false;
int connectAttempts = 3;


void ledBlink(){
  
  delay(500);
  digitalWrite(LED, HIGH);
  delay(500);
  digitalWrite(LED, LOW);
  
}






String httpGETRequest(const char* serverName) {
  HTTPClient http;

  #ifdef DEBUG_REQUESTS
    Serial.println("GET Request");
  #endif
  
  http.begin(serverName);
  
  // Send HTTP GET request
  int httpResponseCode = http.GET();
  
  String payload = "{}"; 
  
  if (httpResponseCode>0) {
    #ifdef DEBUG_REQUESTS
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
    #endif
    
    payload = http.getString();
  }
  else {
    #ifdef DEBUG_REQUESTS
      Serial.print("Error code: ");
      Serial.println(httpResponseCode);
    #endif
  }
  // Free resources
  http.end();

  return payload;
}





String httpPOSTRequest(const char* serverName, String jsonToSend) {
  
  HTTPClient http;
  
  #ifdef DEBUG_REQUESTS
    Serial.println("POST Request");
  #endif

  // Your IP address with path or Domain name with URL path 
  http.begin(serverName);
  http.addHeader("Content-Type", "application/json");

  
  // Send HTTP POST request
  int httpResponseCode = http.POST(jsonToSend);
  
  String payload = "{}"; 
  
  if (httpResponseCode>0) {
    #ifdef DEBUG_REQUESTS
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
    #endif
    payload = http.getString();
  }
  else {
    #ifdef DEBUG_REQUESTS
      Serial.print("Error code: ");
      Serial.println(httpResponseCode);
    #endif
  }
  // Free resources
  http.end();

  return payload;
}



String httpPUTRequest(const char* serverName, String jsonToSend) {
  
  HTTPClient http;
  
  #ifdef DEBUG_REQUESTS
    Serial.println("PUT Request");
  #endif

  // Your IP address with path or Domain name with URL path 
  http.begin(serverName);
  http.addHeader("Content-Type", "application/json");

  
  // Send HTTP POST request
  int httpResponseCode = http.PUT(jsonToSend);
  
  String payload = "{}"; 
  
  if (httpResponseCode>0) {
    #ifdef DEBUG_REQUESTS
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
    #endif
    payload = http.getString();
  }
  else {
    #ifdef DEBUG_REQUESTS
      Serial.print("Error code: ");
      Serial.println(httpResponseCode);
    #endif
  }
  // Free resources
  http.end();

  return payload;
}






//Almacena las credenciales WiFi en la EEPROM
void saveWiFiCredentials(){
  
  int c_index = 0;
  String _ssid(ssid);
  String _pass(password);

  #ifdef DEBUG_WIFI
    Serial.println("Saving credentials: ");
    Serial.print(_ssid.length()); Serial.print(" letters: ");
    Serial.println(_ssid);
  #endif
  
  EEPROM.write(c_index, (int)'W'); c_index++;
  
  //Writes SSID
  
  EEPROM.write(c_index, _ssid.length()); c_index++;
  
  for(int i=0; i<_ssid.length(); i++){
    EEPROM.write(c_index, (int)_ssid[i]); 
    c_index++;
  }

  EEPROM.commit();


  //Writes Password
  
  EEPROM.write(c_index, _pass.length()); c_index++;
  
  for(int i=0; i<_pass.length(); i++){
    EEPROM.write(c_index, (int)_pass[i]); 
    c_index++;
  }

  EEPROM.commit();



  //GUID
  EEPROM.write(c_index, guid.length()); c_index++;
  
  for(int i=0; i<guid.length(); i++){
    EEPROM.write(c_index, (int)guid[i]); 
    c_index++;
  }

  EEPROM.commit();
  isWifiSetUp = true;

  delay(500);


}



//Checks if Wifi networks exists
/*
bool isWifiSettedUp(){
  bool success = true;
  if(strlen(ssid)<1 || strlen(password)<1){
   success = false; 
  }

  #ifdef DEBUG_WIFI
    Serial.println("Wifi set up? "+success);
  #endif
  
  return success;
  
}
*/






//Conexión a la red WiFi, con las credenciales almacenadas en la EEPROM
bool connectWithSavedCredentials(){

 #ifdef DEBUG_WIFI
   Serial.print("SSID: ");
   Serial.println(ssid);
  
   Serial.print("PASS: ");
   Serial.println(password);
 #endif
 
  WiFi.begin(ssid, password);


  ledBlink();
  int att = 5;
  //Serial.println("Connecting to WiFi.. ");
  while ((WiFi.status() != WL_CONNECTED) && (att>0)) {
    delay(500);
    Serial.print(".");
    if(WiFi.status() == WL_CONNECTED){break;}  
    att--;
  }

  if(WiFi.status() == WL_CONNECTED){ 
      Serial.println(WiFi.localIP());
      connectedWifi = true;
      
      udp.begin(localPort);
      
      } else{
      Serial.println("\nNot connected");
      //WiFi.disconnect();
  }


  
}


//Reads from EEPROM WiFi credentials
bool getWiFiCredentials(){
  
  bool success = false;
  int c_index = 0;
  int fb = EEPROM.read(c_index); c_index++;
  
  if(char(fb) == 'W'){ //Wifi setted up
    isWifiSetUp = true;
    Serial.println("WiFi");    
    
    int sz = EEPROM.read(c_index); c_index++;
    String m_ssid = "";

    //SSID
    for(int i=c_index; i<(c_index+sz) ;i++){
      int car = EEPROM.read(i);
      Serial.print(char(car));
      m_ssid += char(car);
    }
    c_index = (c_index + sz); //se actualiza el cursor de lectura de la EPROM


    sz = EEPROM.read(c_index); c_index++;
    String m_pass = "";

    Serial.print(" ");
    
    //PASS
    for(int i=c_index; i<(c_index+sz) ;i++){
      int car = EEPROM.read(i);
      Serial.print(char(car));
      m_pass += char(car);
    }
    c_index = (c_index + sz); //se actualiza el cursor de lectura de la EPROM


    //GUID
    sz = EEPROM.read(c_index); c_index++;
    String m_guid = "";

    Serial.print(" ");    
    for(int i=c_index; i<(c_index+sz) ;i++){
      int car = EEPROM.read(i);
      Serial.print(char(car));
      m_guid += char(car);
    }
    c_index = (c_index + sz); //se actualiza el cursor de lectura de la EPROM

    Serial.print("\n");
     guid = m_guid;

    //Copy the string as a C char array
    
    char c_ssid[m_ssid.length() + 1]; 
    strcpy(c_ssid, m_ssid.c_str());
    
    char c_pass[m_pass.length() + 1]; 
    strcpy(c_pass, m_pass.c_str());
    
    ssid = &c_ssid[0]; 
    password = &c_pass[0]; //Guarda en los punteros de la conexión la posición de memoria del la 1ª posición del array de chars (String)


    credentialsObtained = true; //WIFI CREDENTIALS CLEARLY OBTAINED FROM EEPROM
    
    //Connects
    connectWithSavedCredentials();
    
  }else{
    Serial.println("NOT WiFi");
    }

    //fb = EEPROM.read(1); c_index++;
    //Serial.print("Char: "); Serial.println(char(fb));

  return success;
}







//Intento de conexión a la red WiFi
void attemptToConnect(){

 #ifdef DEBUG_WIFI
   Serial.print("SSID: ");
   Serial.println(ssid);
  
   Serial.print("PASS: ");
   Serial.println(password);
 #endif
 
  WiFi.begin(ssid, password);

  ledBlink();
  
  int att = 5;
  Serial.println("Connecting to WiFi.. ");
  while ((WiFi.status() != WL_CONNECTED) && (att>0)) {
    delay(1000);
    Serial.print(".");
    if(WiFi.status() == WL_CONNECTED){break;}  
    att--;
  }

  if(WiFi.status() == WL_CONNECTED){ 
      connectedWifi = true;
      Serial.println(WiFi.localIP());

       udp.begin(localPort); //Inicia la conexión UDP

      /*
       if(SerialBT.available()) {
        String myString = "[OK]";
        byte buf[] = {'[','O','K',']'};
        myString.toCharArray(buf,4);
        SerialBT.write(buf);
       } */
      //Write on EPROM
      saveWiFiCredentials();

  } else{
      Serial.println("\nNot connected");
      //WiFi.disconnect();

  }
 
}
